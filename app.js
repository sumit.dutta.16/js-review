const express = require("express");
const app = express();
const path = require("path");
require("dotenv").config();

const connection = require("./dbConnection");

connection.connect((err) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Connected to db");
  }
});

const PORT = process.env.PORT || 5000;

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/index.html"));
});

app.get("/matches/:year", async (req, res) => {
  const year = parseInt(req.params.year);
  if (year < 2008 || year > 2017) {
    res.json({
      message: "Not A valid Year Enter between 2007 - 2017",
    });
  } else {
    try {
      const sql = `SELECT * FROM matches WHERE season = '${year}';`;
      const yearData = await getYearData(sql);
      res.json(yearData);
    } catch (err) {
      console.log(err);
      res.json({
        message: err,
      });
    }
  }
});

function getYearData(sql) {
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, results, fields) => {
      if (err) {
        console.log("Error");
        reject("Invalid request made for Year Data");
      } else {
        console.log("Working query");
        resolve(results);
      }
    });
  });
}

app.listen(PORT, () => {
  console.log(`Running on Port ${PORT}`);
});

// 1. Refactor Task 1 with promised based functions. Create a query function which returns a promise. Use this function for the refactoring process.
// . Make the URL generic like GET /matches/<any_year>. It should return matches for only that year
